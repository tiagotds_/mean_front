(function(){
    angular.module('primeiraApp').controller('BillingCycleCtrl', [
        '$http',
        '$location',
        'messages',
        'tabs',
        BillingCycleController
    ]);
    const url = 'http://localhost:3003/api/billingCycles';

    function BillingCycleController($http, $location, messages, tabs) {
        const vm = this;
        const count = 5;
        
        vm.create = () => {
            $http.post(url, vm.billingCycle).then(success).catch(error);
        }
        
        vm.refresh = () => {
            const page = parseInt($location.search().page) || 1;
            $http.get(`${url}?skip=${(page-1) * count}&limit=${count}`).then((response) => {
                vm.billingCycle = {credits: [{}], debts: [{}]};
                vm.billingCycles = response.data;
                vm.calculateValues();
                
                $http.get(`${url}/count`).then((response) => {
                    vm.pages = Math.ceil(response.data.value / count);
                    tabs.show(vm, {tabList: true, tabCreate: true});
                });
            });
        }

        vm.showTabUpdate = (billingCycle) => {
            vm.billingCycle = billingCycle;
            vm.calculateValues();
            tabs.show(vm, {tabUpdate: true});
        }
        
        vm.showTabDelete = (billingCycle) => {
            vm.billingCycle = billingCycle;
            vm.calculateValues();
            tabs.show(vm, {tabDelete: true});
        }

        vm.delete = () => {
            const deleteUrl = `${url}/${vm.billingCycle._id}`;
            $http.delete(deleteUrl).then(success).catch(error);
        }
        
        vm.update = () => {
            const updateUrl = `${url}/${vm.billingCycle._id}`;
            $http.put(updateUrl, vm.billingCycle).then(success).catch(error);
        }

        const success = (response) => {
            vm.refresh();
            messages.addSuccess('Operação realizada com sucesso!');
        }

        const error = (response) => {
            messages.addError(response.data.errors);
        }

        vm.addCredit = (index) => {
            vm.billingCycle.credits.splice(index + 1, 0, {});
        }

        vm.cloneCredit = (index, {name, value}) => {
            vm.billingCycle.credits.splice(index + 1, 0, {name, value});
            vm.calculateValues();
        }

        vm.deleteCredit = (index) => {
            if(vm.billingCycle.credits.length > 1){
                vm.billingCycle.credits.splice(index, 1);
            } else {
                vm.billingCycle.credits = [{}];
            }
            vm.calculateValues();
        }

        vm.addDebt = (index) => {
            vm.billingCycle.debts.splice(index + 1, 0, {});
        }

        vm.cloneDebt = (index, {name, value, status}) => {
            vm.billingCycle.debts.splice(index + 1, 0, {name, value, status});
            vm.calculateValues();
        }

        vm.deleteDebt = (index) => {
            if(vm.billingCycle.debts.length > 1){
                vm.billingCycle.debts.splice(index, 1);
            } else {
                vm.billingCycle.debts = [{}];
            }
            vm.calculateValues();
        }

        vm.calculateValues = () => {
            vm.credit = 0;
            vm.debt = 0;
            if(vm.billingCycle){
                vm.billingCycle.credits.forEach(({value}) => {
                    vm.credit += !value || isNaN(value) ? 0 : parseFloat(value);
                });

                vm.billingCycle.debts.forEach(({value}) => {
                    vm.debt += !value || isNaN(value) ? 0 : parseFloat(value);
                });
            }
            vm.total = vm.credit - vm.debt;
        }

        vm.refresh();
    }
})();